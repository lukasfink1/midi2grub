midi2grub
=========

This is a small python script that creates files or argument strings for the “play” function of grub.
I searched for a similar program. I thought, there is no way, nobody ever wrote such a program. But apparently I couldn’t find one.

So here it is: The program that allows you to hear your favourite music without even completely booting your computer.

It requires python3 and the mido api.

## Usage
The program is pretty simple to use: `$ midi2grub.py [-b] [-t TRACK] [-c CHANNEL] [-m TEMPO] midifile [playseq]`

It at least needs the midi file as argument.
By default it would give you an argument string you can pass to the play function.

The used tempo is either the one given as argument, the first set_tempo found in the midi file or a default value of 120 bpm.
With the -b flag it creates binary files, which can be played by grub too. (By passing the filename as argument to play.)

You can use the -t and -c argument to filter events. For type 2 midis the -t argument is mandatory.

Be aware that the play function is monophonic. You might want to edit your midi files beforehand to avoid unexpected behavior.
By default it plays the last note pressed of all played notes.

**Have fun with your favourite song as init tune.**